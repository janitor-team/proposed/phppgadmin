Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: phpPgAdmin
Upstream-Contact: phppgadmin-devel@lists.sourceforge.net
Source: https://github.com/phppgadmin/phppgadmin/
Files-Excluded: libraries/js
                libraries/adodb
                tests/selenium/selenium-lib/core/lib/cssQuery/cssQuery-p.js
                tests/selenium/selenium-lib/readyState.xpi
                tests/simpletests/data/CFunction

Files: *
Copyright: 2002-2019 The phpPgAdmin Project
License: GPL-2+
Comment: See CREDITS file for list of authors

Files: debian/*
Copyright: 2003-06 Isaac Clerencia <isaac@sindominio.net>
           2005 Piotr Roszatycki <dexter@debian.org>
           2008-09 Giuseppe Iuculano <giuseppe@iuculano.it>
           2008-10 Peter Eisentraut <petere@debian.org>
           2010 Jari Aalto <jari.aalto@cante.net>
           2011-18 Christoph Berg <myon@debian.org>
           2015-19 Jean-Michel Vourgère <nirgal@debian.com>
License: GPL-2+

Files: libraries/highlight.php
Copyright: 2003, 2004, Jacob D. Cohena
License: HIGHLIGHT

Files: xloadtree/*
Copyright: 1999 - 2005 Erik Arvidsson & Emil A Eklund
License: XLOADTREE

Files: tests/selenium/selenium-lib/*
Copyright: 2004-2009 ThoughtWorks, Inc
License: Apache2
Comment: From selenium version 1.0.3, was merged on 2010-03-12.

Files: tests/selenium/selenium-lib/core/lib/cssQuery/*
Copyright: 2004-2005, Dean Edwards (http://dean.edwards.name/)
License: LGPL-2.1

Files: tests/selenium/selenium-lib/core/lib/snapsie.js
Copyright: 2007-2008 Haw-Bin Chai <https://sourceforge.net/u/gyrm>
License: Apache2 or GPL-2
Comment: http://snapsie.sourceforge.net/

Files: tests/selenium/selenium-lib/core/lib/prototype.js
Copyright: 2005 Sam Stephenson <sam@conio.net>
License: MIT

Files: tests/selenium/selenium-lib/core/lib/scriptaculous/*
Copyright: 2005-2008 Thomas Fuchs <thomas@script.aculo.us>
           2005-2008 Marty Haught <mghaught@gmail.com>
           2005 Ivan Krstic (http://blogs.law.harvard.edu/ivan)
           2005 Jon Tirsen (http://www.tirsen.com)
           2005 Sammi Williams (http://www.oriontransfer.co.nz, sammi@oriontransfer.co.nz)
           2005 Michael Schuerig (http://www.schuerig.de/michael/)
License: MIT

Files: tests/selenium/selenium-lib/core/xpath/*
Copyright: 2007 Cybozu Labs, Inc.
License: MIT
Comment: http://coderepos.org/share/wiki/JavaScript-XPath

Files: tests/selenium/selenium-lib/core/xpath/dom.js
       tests/selenium/selenium-lib/core/xpath/util.js
       tests/selenium/selenium-lib/core/xpath/xmltoken.js
       tests/selenium/selenium-lib/core/xpath/xpath.js
Copyright: 2005 Google Inc.
License: BSDGOOGLE
Comment: http://goog-ajaxslt.sourceforge.net/

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the text of the GNU General Public License version 2
 can be found in `/usr/share/common-licenses/GPL-2'.

License: XLOADTREE
 This software is provided "as is", without warranty of any kind, express or
 implied, including  but not limited  to the warranties of  merchantability,
 fitness for a particular purpose and noninfringement. In no event shall the
 authors or  copyright  holders be  liable for any claim,  damages or  other
 liability, whether  in an  action of  contract, tort  or otherwise, arising
 from,  out of  or in  connection with  the software or  the  use  or  other
 dealings in the software.
 .
 This  software is  available under the  three different licenses  mentioned
 below.  To use this software you must chose, and qualify, for one of those.
 .
 The WebFX Non-Commercial License          http://webfx.eae.net/license.html
 Permits  anyone the right to use the  software in a  non-commercial context
 free of charge.
 .
 The WebFX Commercial license           http://webfx.eae.net/commercial.html
 Permits the  license holder the right to use  the software in a  commercial
 context. Such license must be specifically obtained, however it's valid for
 any number of  implementations of the licensed software.
 .
 GPL - The GNU General Public License    http://www.gnu.org/licenses/gpl.txt
 Permits anyone the right to use and modify the software without limitations
 as long as proper  credits are given  and the original  and modified source
 code are included. Requires  that the final product, software derivate from
 the original  source or any  software  utilizing a GPL  component, such  as
 this, is also licensed under the GPL license.

License: HIGHLIGHT
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 Neither the name of Jacob D. Cohen nor the names of his contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Apache2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use these files except in compliance with the License.
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the text of the Apache License version 2
 can be found in `/usr/share/common-licenses/Apache-2.0'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the text of the GNU Lesser General Public License version
 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the text of the GNU General Public License version 2
 can be found in `/usr/share/common-licenses/GPL-2'.

License: BSDGOOGLE
 Redistribution and use in source and binary forms, with or without
 Modification, are permitted provided that the following conditions are
 Met:
 .        
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the
    distribution.
 .
  * Neither the name of Google Inc. nor the names of its contributors
    may be used to endorse or promote products derived from this
    software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
Comment: http://goog-ajaxslt.sourceforge.net/COPYING
