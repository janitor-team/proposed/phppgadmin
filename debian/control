Source: phppgadmin
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders: Jean-Michel Vourgère <nirgal@debian.org>
Section: web
Priority: optional
Build-Depends: debhelper-compat (= 13), dh-apache2, pkg-php-tools (>= 1.7~)
Standards-Version: 4.5.1
Homepage: https://github.com/phppgadmin/phppgadmin/
Vcs-Browser: https://salsa.debian.org/postgresql/phppgadmin
Vcs-Git: https://salsa.debian.org/postgresql/phppgadmin.git
Rules-Requires-Root: no

Package: phppgadmin
Architecture: all
Pre-Depends: ${misc:Pre-Depends}
Depends: libapache2-mod-php | php-cgi | php-fpm,
         libjs-jquery,
         libphp-adodb,
         ${misc:Depends},
         ${phpcomposer:Debian-require}
Recommends: ${misc:Recommends}, ${phpcomposer:Debian-recommend}
Suggests: postgresql, postgresql-doc, slony1-bin, ${phpcomposer:Debian-suggest}
Description: web-based administration tool for PostgreSQL
 phpPgAdmin is a web-based administration tool for PostgreSQL. It is perfect
 for PostgreSQL DBAs, newbies and hosting services.
 .
 Features:
  * Administer multiple servers
  * Manage all aspects of:
    * Users & groups
    * Databases
    * Schemas
    * Tables, indexes, constraints, triggers, rules & privileges
    * Views, sequences & functions
    * Advanced objects
    * Reports
  * Easy data manipulation:
    * Browse tables, views & reports
    * Execute arbitrary SQL
    * Select, insert, update and delete
  * Dump table data in a variety of formats: SQL, COPY, XML, XHTML, CSV, Tabbed,
    pg_dump
  * Import SQL scripts, COPY data, XML, CSV and Tabbed
  * Supports the Slony master-slave replication engine
  * Excellent language support:
    * Available in 27 languages
    * No encoding conflicts. Edit Russian data using a Japanese interface!
  * Easy to install and configure
